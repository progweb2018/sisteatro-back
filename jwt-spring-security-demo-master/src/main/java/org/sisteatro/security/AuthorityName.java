package org.sisteatro.security;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}
