package org.sisteatro.domain.venda;

import com.sun.xml.internal.bind.v2.model.core.ID;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "venda")

public class Venda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "venda_id_seq")
    @SequenceGenerator(name = "venda_id_seq", sequenceName = "venda_id_seq", allocationSize = 1)
    private long id;

    @Column(name = "ACESSOPNE")
    private Boolean acessoPNE = Boolean.FALSE;

}
