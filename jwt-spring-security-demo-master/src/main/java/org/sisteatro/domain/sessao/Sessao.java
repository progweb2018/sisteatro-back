package org.sisteatro.domain.sessao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.sisteatro.domain.sala.Sala;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "sessao")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Sessao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sessao_id_seq")
    @SequenceGenerator(name = "sessao_id_seq", sequenceName = "sessao_id_seq", allocationSize = 1)
    private long id;

    @Column(name = "data", length = 50)
    private LocalDateTime data;

    @Column(name = "horario", length = 50)
    private LocalDateTime horario;

    @NotNull
    @Column(name = "preco", length = 50)
    private Long preco;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sala_id", referencedColumnName = "id")
    private Sala sala;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public LocalDateTime getHorario() {
        return horario;
    }

    public void setHorario(LocalDateTime horario) {
        this.horario = horario;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(Long preco) {
        this.preco = preco;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }
}
