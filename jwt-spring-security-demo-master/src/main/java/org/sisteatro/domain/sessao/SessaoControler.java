package org.sisteatro.domain.sessao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@JsonIgnoreProperties("horario, data")
@RequestMapping("/sessao")
@CrossOrigin(origins = "http://localhost:4200")
public class SessaoControler {

    @Autowired
    private SessaoService sessaoService;

    @PostMapping
    public ResponseEntity<?> salvar(@Validated @RequestBody Sessao sessao){
        return new ResponseEntity(sessaoService.save(sessao), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> alterar(@Validated @RequestBody Sessao sessao){
        return new ResponseEntity(sessaoService.save(sessao), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{sessaoId}")
    public ResponseEntity<?> excluir(@PathVariable Long sessaoId) {
        return new ResponseEntity(sessaoService.delete(sessaoId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> findAll() {
        return new ResponseEntity(sessaoService.findAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/{sessaoId}")
    public ResponseEntity<?> findById(@PathVariable Long sessaoId) {
        return new ResponseEntity(sessaoService.findById(sessaoId), HttpStatus.OK);
    }

}
