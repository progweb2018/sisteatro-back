package org.sisteatro.domain.sessao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SessaoService {

    @Autowired
    private SessaoRepository sessaoRepository;


    public Sessao save(Sessao sessao){
        return sessaoRepository.save(sessao);
    }

    public boolean delete(Long produtoId){
        Sessao sessaoBusca = sessaoRepository.getOne(produtoId);

        if( sessaoBusca != null ){
            sessaoRepository.delete(sessaoBusca);
            return true;
        }
        else {
            return false;
        }
    }

    public Sessao findById(Long produtoId){
        Sessao sessao = sessaoRepository.getOne(produtoId);
        System.out.println(sessao);
        return sessao;
    }

    public List<Sessao> findAll(){
        return sessaoRepository.findAll();
    }

}
