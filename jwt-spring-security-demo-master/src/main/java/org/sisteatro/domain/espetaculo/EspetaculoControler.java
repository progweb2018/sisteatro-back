package org.sisteatro.domain.espetaculo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;

@RestController
@RequestMapping("/espetaculo")
@CrossOrigin(origins = "http://localhost:4200")
public class EspetaculoControler {

    @Autowired
    private EspetaculoService espetaculoService;

    @PostMapping
    public ResponseEntity<?> salvar (@Validated @RequestBody Espetaculo espetaculo){
        return new ResponseEntity(espetaculoService.save(espetaculo), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<?> findAll() {
        return new ResponseEntity(espetaculoService.findAll(), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> alterar (@Validated @RequestBody Espetaculo espetaculo){
        return new ResponseEntity(espetaculoService.save(espetaculo), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{espetaculoId}")
    public ResponseEntity<?> excluir (@PathVariable Long espetaculoId){
        return new ResponseEntity(espetaculoService.delete(espetaculoId), HttpStatus.OK);
    }

    @GetMapping(value = "/{espetaculoId}")
    public ResponseEntity<?> findById(@PathVariable Long espetaculoId){
        return new ResponseEntity(espetaculoService.findById(espetaculoId), HttpStatus.OK);
    }

//    @GetMapping(value = "/findByName/{nomeEspetaculo}")
//    public ResponseEntity<?> findById(@PathVariable String nomeEspetaculo){
//        return new ResponseEntity(espetaculoService.findByIdByNome(nomeEspetaculo), HttpStatus.OK)
//    }



}
