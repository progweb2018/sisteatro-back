package org.sisteatro.domain.espetaculo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EspetaculoService {

    @Autowired
    private EspetaculoRepository espetaculoRepository;

    public Espetaculo save(Espetaculo espetaculo){ return espetaculoRepository.save(espetaculo); }

    public boolean delete (long espetaculoId) {
        Espetaculo espetaculoBusca = espetaculoRepository.getOne(espetaculoId);
        if ( espetaculoBusca != null ){
            espetaculoRepository.delete(espetaculoBusca);
            return true;
        }
        else {
            return false;
        }
    }

    public Espetaculo findById (Long espetaculoId){
        return espetaculoRepository.getOne(espetaculoId);
    }

    public List<Espetaculo> findAll(){
        return espetaculoRepository.findAll();
    }

    public List<Espetaculo> findEspetaculoByNome(String nome){
        return espetaculoRepository.findEspetaculoByNomeIgnoreCase(nome);
    }
}
