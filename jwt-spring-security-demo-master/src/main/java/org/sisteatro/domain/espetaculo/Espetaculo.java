package org.sisteatro.domain.espetaculo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Table(name = "espetaculo")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "sessoes"})
public class Espetaculo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "espetaculo_id_seq")
    @SequenceGenerator(name = "espetaculo_id_seq", sequenceName = "espetaculo_id_seq", allocationSize = 1)
    private long id;

    @NotNull
    @Column(name = "nome", length = 50)
    private String nome;

    @NotNull
    @Column (name = "genero", length = 50)
    private String genero;

    @Column (name = "data", length = 11)
    private Date data;

    @Column(name = "sinopse", length = 80)
    private String sinopse;

    @Column(name = "classificacao", length = 20)
    private String classificacao;

    @Column (name = "duracao", length = 20)
    private LocalTime duracao;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    public LocalTime getDuracao() {
        return duracao;
    }

    public void setDuracao(LocalTime duracao) {
        this.duracao = duracao;
    }
}
