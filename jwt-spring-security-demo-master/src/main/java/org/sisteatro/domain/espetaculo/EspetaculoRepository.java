package org.sisteatro.domain.espetaculo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspetaculoRepository extends JpaRepository<Espetaculo, Long> {

    List<Espetaculo> findEspetaculoByNomeIgnoreCase (String nome);
}
