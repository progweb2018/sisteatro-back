package org.sisteatro.domain.sala;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalaService {

    @Autowired
    private SalaRepository salaRepository;


    public Sala save(Sala sala){
        return salaRepository.save(sala);
    }

    public boolean delete(Long produtoId){
        Sala salaBusca = salaRepository.getOne(produtoId);
        if( salaBusca != null ){
            salaRepository.delete(salaBusca);
            return true;
        }
        else {
            return false;
        }
    }

    public Sala findById(Long produtoId){
        return salaRepository.getOne(produtoId);
    }

    public List<Sala> findAll(){
        return salaRepository.findAll();
    }

    public List<Sala> findSalaByNome (String nome) {
        return salaRepository.findSalasByNomeIgnoreCase(nome);
    }

}
