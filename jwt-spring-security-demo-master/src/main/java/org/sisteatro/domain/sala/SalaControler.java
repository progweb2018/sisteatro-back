package org.sisteatro.domain.sala;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sala")
@CrossOrigin(origins = "http://localhost:4200")
public class SalaControler {

    @Autowired
    private SalaService salaService;

    @PostMapping
//    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> salvar(@Validated @RequestBody Sala sala){
        return
                new ResponseEntity(salaService.save(sala), HttpStatus.CREATED);
    }

    @PutMapping
//    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> alterar(@Validated @RequestBody Sala sala){
        return new ResponseEntity(salaService.save(sala), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{salaId}")
    public ResponseEntity<?> excluir(@PathVariable Long salaId) {
        return new ResponseEntity(salaService.delete(salaId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> findAll() {
        return new ResponseEntity(salaService.findAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/{salaId}")
    public ResponseEntity<?> findById(@PathVariable Long salaId) {
        return new ResponseEntity(salaService.findById(salaId), HttpStatus.OK);
    }
//Alterei este abaixo o primeiro find, estava findById agora esta findByIdByNome
    @GetMapping(value = "/findByName/{nomeSala}")
    public ResponseEntity<?> findByIdByNome(@PathVariable String nomeSala) {
        return new ResponseEntity(salaService.findSalaByNome(nomeSala), HttpStatus.OK);
    }
}
