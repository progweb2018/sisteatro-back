package org.sisteatro.domain.sala;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.sisteatro.domain.sessao.Sessao;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sala")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "sessoes"})
public class Sala implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sala_id_seq")
    @SequenceGenerator(name = "sala_id_seq", sequenceName = "sala_id_seq", allocationSize = 1)
    private long id;

    @Column(name = "NOME", length = 50)
    private String nome;

    @Column(name = "TOTALCADEIRAS", length = 200)
    private Integer totalCadeiras;

    @Column(name = "ACESSOPNE")
    private Boolean acessoPNE = Boolean.FALSE;

    @Column(name = "CAMAROTE")
    private Boolean camarote = Boolean.FALSE;

    @Column(name = "TOTALCADEIRASCAMAROTE")
    private Integer totalCadeirasCamarote;

    @Column(name = "DESCRICAO")
    private String descricao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "sala", cascade = CascadeType.ALL)
    private List<Sessao> sessoes;


    /** GETTERS AND SETTERS*/

    public List<Sessao> getSessoes() {
        return sessoes;
    }

    public void setSessoes(List<Sessao> sessoes) {
        this.sessoes = sessoes;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getTotalCadeiras() {
        return totalCadeiras;
    }

    public void setTotalCadeiras(Integer totalCadeiras) {
        this.totalCadeiras = totalCadeiras;
    }

    public Boolean getAcessoPNE() {
        return acessoPNE;
    }

    public void setAcessoPNE(Boolean acessoPNE) {
        this.acessoPNE = acessoPNE;
    }

    public Boolean getCamarote() {
        return camarote;
    }

    public void setCamarote(Boolean camarote) {
        this.camarote = camarote;
    }

    public Integer getTotalCadeirasCamarote() {
        return totalCadeirasCamarote;
    }

    public void setTotalCadeirasCamarote(Integer totalCadeirasCamarote) {
        this.totalCadeirasCamarote = totalCadeirasCamarote;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
