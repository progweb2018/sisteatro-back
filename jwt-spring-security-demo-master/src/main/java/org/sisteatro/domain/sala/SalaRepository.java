package org.sisteatro.domain.sala;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SalaRepository extends JpaRepository<Sala, Long> {

    List<Sala> findSalasByNomeIgnoreCase(String nome);


//    @Modifying
//    @Transactional
//    @Query("UPDATE #{#entityName} SET situacao =:situacao WHERE id =:id")
//    public void alterSituacao(@Param("id") long id,
//                              @Param("situacao") SituacaoEnum situacao);
//
//    @Query(value = "SELECT * FROM disciplina " +
//            "WHERE id <> :id " +
//            "AND TRIM(unaccent(lower(codigo))) = TRIM((lower(:codigo))) " +
//            "AND unidade_id = :unidade_id", nativeQuery = true)
//    public List<Disciplina> findDisciplinaByIdAndCodigoAndUnidadeId(@Param("id") long id,
//                                                                    @Param("codigo") String codigo,
//                                                                    @Param("unidade_id") long unidade_id);


}
