DROP TABLE IF EXISTS sala;

CREATE TABLE sala
(
id int PRIMARY KEY,
nome VARCHAR(50) NOT NULL,
totalCadeiras VARCHAR(100) NOT NULL,
acessoPNE boolean,
camarote boolean,
totalCadeirasCamarote VARCHAR(50),
descricao VARCHAR(50)
--CONSTRAINT sala_pkey PRIMARY KEY (id)
);

INSERT INTO sala (id, nome, totalCadeiras, acessoPNE, camarote, descricao) VALUES (1, 'Sala 1', 50, false, false, 'Primeira sala do teatro');


DROP TABLE IF EXISTS public.sessao;

CREATE TABLE public.sessao
(
    id int PRIMARY KEY,
    data timestamp,
    horario time,
    preco bigint NOT NULL,
    sala_id int NOT NULL,
    CONSTRAINT sala_id FOREIGN KEY (sala_id) REFERENCES sala (id)
);

INSERT INTO public.sessao (id, data, preco, sala_id) VALUES (1, '2018-07-15 19:10:25-07', 50, 1);
INSERT INTO public.sessao (id, data, preco, sala_id) VALUES (2, '2018-07-18 19:10:25-07', 60, 1);

CREATE TABLE public.espetaculo
(
    id int PRIMARY KEY,
    nome VARCHAR(50) NOT NULL,
    genero VARCHAR(50) NOT NULL,
    data timestamp,
    sinopse VARCHAR(80),
    classificacao VARCHAR(20),
    duracao time
);

INSERT INTO public.espetaculo (id, nome, genero, data, sinopse, classificacao) VALUES (1, 'O grande espetaculo', 'ação', '2018-07-15 19:10:25-07', 'será um belo espetaculo', '18+');



